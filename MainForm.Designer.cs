﻿namespace ListUsers
{
    partial class MainForm
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.AddUserButton = new System.Windows.Forms.Button();
            this.SearchUserButton = new System.Windows.Forms.Button();
            this.SortDescendingButton = new System.Windows.Forms.Button();
            this.SortAscendingButton = new System.Windows.Forms.Button();
            this.usersListView = new System.Windows.Forms.ListView();
            this.totalUsersLabel = new System.Windows.Forms.Label();
            this.ShowErrosButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(82, 6);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(138, 20);
            this.firstNameTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Sobrenome:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Email";
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.Location = new System.Drawing.Point(82, 41);
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(138, 20);
            this.lastNameTextBox.TabIndex = 4;
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(82, 81);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(138, 20);
            this.emailTextBox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Pesquisar:";
            // 
            // searchTextBox
            // 
            this.searchTextBox.Location = new System.Drawing.Point(82, 122);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(138, 20);
            this.searchTextBox.TabIndex = 7;
            // 
            // AddUserButton
            // 
            this.AddUserButton.Location = new System.Drawing.Point(235, 6);
            this.AddUserButton.Name = "AddUserButton";
            this.AddUserButton.Size = new System.Drawing.Size(126, 23);
            this.AddUserButton.TabIndex = 8;
            this.AddUserButton.Text = "Adicionar usuario";
            this.AddUserButton.UseVisualStyleBackColor = true;
            this.AddUserButton.Click += new System.EventHandler(this.AddUserButton_Click);
            // 
            // SearchUserButton
            // 
            this.SearchUserButton.Location = new System.Drawing.Point(235, 122);
            this.SearchUserButton.Name = "SearchUserButton";
            this.SearchUserButton.Size = new System.Drawing.Size(126, 23);
            this.SearchUserButton.TabIndex = 9;
            this.SearchUserButton.Text = "Procurar usuario";
            this.SearchUserButton.UseVisualStyleBackColor = true;
            this.SearchUserButton.Click += new System.EventHandler(this.SearchUserButton_Click);
            // 
            // SortDescendingButton
            // 
            this.SortDescendingButton.Location = new System.Drawing.Point(235, 81);
            this.SortDescendingButton.Name = "SortDescendingButton";
            this.SortDescendingButton.Size = new System.Drawing.Size(126, 23);
            this.SortDescendingButton.TabIndex = 10;
            this.SortDescendingButton.Text = "Ordem decrescente";
            this.SortDescendingButton.UseVisualStyleBackColor = true;
            this.SortDescendingButton.Click += new System.EventHandler(this.SortDescendingButton_Click);
            // 
            // SortAscendingButton
            // 
            this.SortAscendingButton.Location = new System.Drawing.Point(235, 41);
            this.SortAscendingButton.Name = "SortAscendingButton";
            this.SortAscendingButton.Size = new System.Drawing.Size(126, 23);
            this.SortAscendingButton.TabIndex = 11;
            this.SortAscendingButton.Text = "Ordem crescente";
            this.SortAscendingButton.UseVisualStyleBackColor = true;
            this.SortAscendingButton.Click += new System.EventHandler(this.SortAscendingButton_Click);
            // 
            // usersListView
            // 
            this.usersListView.HideSelection = false;
            this.usersListView.Location = new System.Drawing.Point(12, 151);
            this.usersListView.Name = "usersListView";
            this.usersListView.Size = new System.Drawing.Size(349, 222);
            this.usersListView.TabIndex = 12;
            this.usersListView.UseCompatibleStateImageBehavior = false;
            // 
            // totalUsersLabel
            // 
            this.totalUsersLabel.AutoSize = true;
            this.totalUsersLabel.Location = new System.Drawing.Point(106, 376);
            this.totalUsersLabel.Name = "totalUsersLabel";
            this.totalUsersLabel.Size = new System.Drawing.Size(154, 13);
            this.totalUsersLabel.TabIndex = 13;
            this.totalUsersLabel.Text = "Total de usuarios registrados: 0";
            // 
            // ShowErrosButton
            // 
            this.ShowErrosButton.Location = new System.Drawing.Point(12, 406);
            this.ShowErrosButton.Name = "ShowErrosButton";
            this.ShowErrosButton.Size = new System.Drawing.Size(349, 32);
            this.ShowErrosButton.TabIndex = 14;
            this.ShowErrosButton.Text = "Mostrar erros";
            this.ShowErrosButton.UseVisualStyleBackColor = true;
            this.ShowErrosButton.Click += new System.EventHandler(this.ShowErrosButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 450);
            this.Controls.Add(this.ShowErrosButton);
            this.Controls.Add(this.totalUsersLabel);
            this.Controls.Add(this.usersListView);
            this.Controls.Add(this.SortAscendingButton);
            this.Controls.Add(this.SortDescendingButton);
            this.Controls.Add(this.SearchUserButton);
            this.Controls.Add(this.AddUserButton);
            this.Controls.Add(this.searchTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.lastNameTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.firstNameTextBox);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.Button AddUserButton;
        private System.Windows.Forms.Button SearchUserButton;
        private System.Windows.Forms.Button SortDescendingButton;
        private System.Windows.Forms.Button SortAscendingButton;
        private System.Windows.Forms.ListView usersListView;
        private System.Windows.Forms.Label totalUsersLabel;
        private System.Windows.Forms.Button ShowErrosButton;
    }
}

