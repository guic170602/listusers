﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListUsers
{
    public partial class MainForm : Form
    {
        private List<User> users = new List<User>();
        private Stack<string> errorStack = new Stack<string>();

        public MainForm()
        {
            InitializeComponent();
            InitializeObserver();
        }

        private void InitializeObserver()
        {
            UserNotifier.Instance.UserAction += HandleUserAction;
        }

        private void HandleUserAction(string action)
        {
            MessageBox.Show($"Ação: {action}");
        }

        private void UpdateListView()
        {
            usersListView.Items.Clear();
            foreach (var user in users)
            {
                ListViewItem item = new ListViewItem(user.FirstName);
                item.SubItems.Add(user.LastName);
                item.SubItems.Add(user.Email);
                usersListView.Items.Add(item);
            }


            totalUsersLabel.Text = $"Total de usuarios registrados: {users.Count}";
        }


        private void AddUserButton_Click(object sender, EventArgs e)
        {
            try
            {
                string firstName = firstNameTextBox.Text;
                string lastName = lastNameTextBox.Text;
                string email = emailTextBox.Text;

                if (string.IsNullOrWhiteSpace(firstName) || string.IsNullOrWhiteSpace(lastName) || string.IsNullOrWhiteSpace(email))
                {
                    throw new ArgumentException("Por favor preencha todos os campos.");
                }

                users.Add(new User(firstName, lastName, email));
                UpdateListView();
                UserNotifier.Instance.Notify($"Usuario: '{firstName} {lastName}' adicionado.");
            }
            catch (Exception ex)
            {
                errorStack.Push($"Erro: {ex.Message} - {DateTime.Now}");
            }

        }

        private void SearchUserButton_Click(object sender, EventArgs e)
        {
            try
            {
                string searchTerm = searchTextBox.Text;
                if (string.IsNullOrWhiteSpace(searchTerm))
                {
                    throw new ArgumentException("Por favor insira um email para pesquisar.");
                }

                var user = users.FirstOrDefault(u => u.Email == searchTerm);
                if (user == null)
                {
                    MessageBox.Show("Usuario não encontrado.");
                    return;
                }

                // Display user details
                MessageBox.Show($"Nome: {user.FirstName} {user.LastName}\nEmail: {user.Email}");
            }
            catch (Exception ex)
            {
                errorStack.Push($"Erro: {ex.Message} - {DateTime.Now}");
            }
        }

        private void SortAscendingButton_Click(object sender, EventArgs e)
        {
            users = users.OrderBy(u => u.Email).ToList();
            UpdateListView();
            UserNotifier.Instance.Notify("Usuarios ordenado de forma crescente.");
        }

        private void SortDescendingButton_Click(object sender, EventArgs e)
        {
            users = users.OrderByDescending(u => u.Email).ToList();
            UpdateListView();
            UserNotifier.Instance.Notify("Usuarios ordenado de forma decrescente.");
        }

        private void ShowErrosButton_Click(object sender, EventArgs e)
        {
            if (errorStack.Count == 0)
            {
                MessageBox.Show("Nenhum erro a mostrar.");
                return;
            }

            var errorMessage = string.Join("\n", errorStack);
            MessageBox.Show(errorMessage);
        }
    }
}
