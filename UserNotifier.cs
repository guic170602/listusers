﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListUsers
{
    public class UserNotifier
    {
        private static UserNotifier instance;
        public static UserNotifier Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserNotifier();
                }
                return instance;
            }
        }

        private UserNotifier() { }

        public event Action<string> UserAction;

        public void Notify(string action)
        {
            UserAction?.Invoke(action);
        }
    }

}
